package com.example.multi_defaults;

public interface InterfaceB {
    default void defaultMethod() {
        System.out.println("インタフェースBのメソッドが呼び出されました。");
    }
}
