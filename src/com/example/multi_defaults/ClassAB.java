package com.example.multi_defaults;

public class ClassAB implements InterfaceA, InterfaceB{
    @Override
    public void defaultMethod() {
        InterfaceA.super.defaultMethod();
        InterfaceB.super.defaultMethod();
    }
}
