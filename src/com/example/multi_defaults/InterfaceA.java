package com.example.multi_defaults;

interface InterfaceA {
    default void defaultMethod() {
        System.out.println("インタフェースAのメソッドが呼び出されました。");
    }
}
