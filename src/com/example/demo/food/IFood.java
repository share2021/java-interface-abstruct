package com.example.demo.food;

public interface IFood {
    /**
     * 買う
     *
     * @param number 購入個数
     */
    void buy(int number);

    /**
     * 料理する
     *
     * @return 成功:true, 失敗:false
     */
    boolean cook();

    /**
     * 食べる
     *
     * @param amount 食べる量
     */
    void eat(int amount);

    /**
     * 配達
     * 対応していない場合は例外発生
     */
    default void deliver()
            throws UnsupportedOperationException {
        throw new UnsupportedOperationException("deliver");
    }
}
