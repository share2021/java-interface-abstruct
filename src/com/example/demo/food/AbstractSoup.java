package com.example.demo.food;

public abstract class AbstractSoup implements IFood {
    private int water;
    private int salt;
    private int stock;

    public AbstractSoup(int water, int salt){
        this.water = water;
        this.salt = salt;
    }

    protected abstract boolean cut();

    protected boolean stew() {
        if(water >= 1 && salt >= 1){
            System.out.println("煮込みます。");
            water--;
            salt--;
            return true;
        }
        System.out.println("材料が足りません。");
        return false;
    }

    protected abstract boolean decorate();

    @Override
    public boolean cook() {
        System.out.println("スープを作ります。");
        if (cut()) {
            if (stew()) {
                if (decorate()) {
                    System.out.println("完成しました。");
                    stock++;
                    return true;
                }
            }
        }
        System.out.println("スープ作りに失敗しました。");
        return false;
    }

    @Override
    public void buy(int number) {
        if (number > 0) {
            System.out.println("スープを" + number + "人分買いました");
            stock += number;
        }
    }

    @Override
    public void eat(int amount) {
        if (amount >= 1 && stock >= amount){
            stock -= amount;
        }
    }
}
