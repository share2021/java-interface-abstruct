package com.example.demo.food;

public class WhiteBread extends AbstractBread{

    public WhiteBread(int flour, int salt, int yeast, int water) {
        super(flour, salt, yeast, water);
        System.out.println("食パンの材料を準備します。");
    }

    @Override
    protected boolean bake() {
        System.out.println("焼きます。");
        return true;
    }

    @Override
    protected boolean decorate() {
        return true;
    }

    @Override
    public boolean cook() {
        System.out.println("食パンを作ります。");
        return super.cook();
    }

    @Override
    public void eat(int amount) {
        System.out.println("食パンを食べます。");
        super.eat(amount);
    }

    @Override
    public void buy(int number) {
        if (number > 0) {
            System.out.print("食");
            super.buy(number);
        }
    }
}
