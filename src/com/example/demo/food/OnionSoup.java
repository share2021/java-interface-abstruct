package com.example.demo.food;

public class OnionSoup extends AbstractSoup{
    private int onion;
    private int bouillon;
    private int parsley;

    public OnionSoup(int water, int salt, int onion, int bouillon, int parsley) {
        super(water, salt);
        this.onion = onion;
        this.bouillon = bouillon;
        this.parsley = parsley;
        System.out.println("オニオンスープの材料を準備します。");
    }

    @Override
    protected boolean cut() {
        if (onion >= 1) {
            System.out.println("玉ねぎを切ります。");
            onion--;
            return true;
        }
        return false;
    }

    @Override
    protected boolean stew() {
        if (bouillon >= 1) {
            if (super.stew()) {
                bouillon--;
                return true;
            }
        }else {
            System.out.println("材料が足りません。");
        }
        return false;
    }

    @Override
    protected boolean decorate() {
        if(parsley >= 1){
            System.out.println("パセリを散らします。");
            parsley--;
        }
        return true;
    }

    @Override
    public boolean cook() {
        System.out.print("オニオン");
        return super.cook();
    }
}
