package com.example.demo.food;

public class CreamBun extends AbstractBread{
    private int cream;

    public CreamBun(int flour, int salt, int yeast, int water, int cream) {
        super(flour, salt, yeast, water);
        this.cream = cream;
        System.out.println("クリームパンの材料を準備します。");
    }

    @Override
    protected boolean bake() {
        System.out.println("焼きます。");
        return true;
    }

    @Override
    protected boolean decorate() {
        if (cream >= 1){
            System.out.println("クリームを注入します。");
            cream--;
            return true;
        }
        System.out.println("クリームが足りません。");
        return false;
    }

    @Override
    public boolean cook() {
        System.out.println("クリームパンを作ります。");
        return super.cook();
    }

    @Override
    public void eat(int amount) {
        System.out.println("クリームパンを食べます。");
        super.eat(amount);
    }

    @Override
    public void buy(int number) {
        if (number > 0) {
            System.out.print("クリーム");
            super.buy(number);
        }
    }
}
