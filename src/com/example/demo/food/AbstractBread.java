package com.example.demo.food;

public abstract class AbstractBread implements IFood {
    private int flour;
    private int salt;
    private int yeast;
    private int water;
    private int stock = 0;

    public AbstractBread(int flour, int salt, int yeast, int water) {
        this.flour = flour;
        this.salt = salt;
        this.yeast = yeast;
        this.water = water;
    }

    /**
     * パン生地を作る
     *
     * @return 成功:true, 材料不足:false
     */
    protected boolean makeDough() {
        System.out.println("生地を作ります。");
        if (flour >= 1
                && salt >= 1
                && yeast >= 1
                && water >= 1) {
            flour--;
            salt--;
            yeast--;
            water--;
            return true;
        } else {
            System.out.println("材料が足りません。");
            return false;
        }
    }

    /**
     * パンを焼く
     *
     * @return 成功:true, 温度・時間が不適切:false
     */
    protected abstract boolean bake();

    /**
     * パンを焼いた後の飾りつけ
     *
     * @return 成功:true, 材料不足:false
     */
    protected abstract boolean decorate();

    @Override
    public void buy(int number) {
        if (number > 0) {
            System.out.println("パンを" + number + "個買いました");
            stock += number;
        }
    }

    @Override
    public boolean cook() {
        if (makeDough()
                && bake()
                && decorate()) {
            System.out.println("完成しました。");
            stock++;
            return true;
        }
        System.out.println("パン作りに失敗しました。");
        return false;
    }

    @Override
    public void eat(int amount) {
        if(amount >= 1 && stock >= amount) {
            stock -= amount;
        }
    }

    @Override
    public void deliver() {
        if(stock >= 1) {
            System.out.println("配達します。");
            stock--;
        }
    }
}
