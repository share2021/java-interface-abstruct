package com.example.demo;

import com.example.demo.food.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<IFood> menu = new ArrayList<>();
        menu.add(new WhiteBread(1, 1, 1, 1));
        menu.add(new CreamBun(1, 1, 1, 1, 1));
        menu.add(new OnionSoup(1, 1, 1, 1, 1));
        for (IFood food : menu){
            System.out.println("---------------------------------------------------------");
            food.cook();
            try {
                food.deliver();
            } catch (UnsupportedOperationException e) {
                System.out.println("配達できません。");
            }
        }
    }
}
