package com.example.regexp.email;

public class Main {
    // from org.hibernate.validator.internal.constraintvalidators.AbstractEmailValidator<A extends Annotation>
    private static final String LOCAL_PART_ATOM = "[a-z0-9!#$%&'*+/=?^_`{|}~\\u0080-\\uFFFF-]";
    private static final String LOCAL_PART_INSIDE_QUOTES_ATOM = "([a-z0-9!#$%&'*.(),<>\\[\\]:;  @+/=?^_`{|}~\\u0080-\\uFFFF-]|\\\\\\\\|\\\\\\\")";
    private static final String LOCAL = "(" + LOCAL_PART_ATOM + "+|\"" + LOCAL_PART_INSIDE_QUOTES_ATOM + "+\")" +
            "(\\." + "(" + LOCAL_PART_ATOM + "+|\"" + LOCAL_PART_INSIDE_QUOTES_ATOM + "+\")" + ")*";

    // from org.hibernate.validator.internal.util.DomainNameUtil
    private static final String DOMAIN_CHARS_WITHOUT_DASH = "[a-z\\u0080-\\uFFFF0-9!#$%&'*+/=?^_`{|}~]";
    private static final String DOMAIN_LABEL = "(" + DOMAIN_CHARS_WITHOUT_DASH + "-*)*" + DOMAIN_CHARS_WITHOUT_DASH + "+";
    private static final String DOMAIN = DOMAIN_LABEL + "+(\\." + DOMAIN_LABEL + "+)*";

    public static void main(String[] args) {
        String mail = "aaa@aaa.com";
        int splitPosition = mail.lastIndexOf('@');  // splitPosition = 3
        System.out.println(mail.substring(0, splitPosition));
        System.out.println(mail.substring(splitPosition + 1));
        System.out.println("LOCAL: " + LOCAL);
        System.out.println("DOMAIN: " + DOMAIN);
    }
}
